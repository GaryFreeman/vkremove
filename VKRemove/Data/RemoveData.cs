﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VKRemove.Data
{
	public class RemoveData
	{
		public int Interval
		{
			get;
			set;
		}

		public bool PhotosLikes
		{
			get;
			set;
		}

		public bool PostsLikes
		{
			get;
			set;
		}

		public bool SavedPhotos
		{
			get;
			set;
		}
	}
}
