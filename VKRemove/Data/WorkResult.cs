﻿namespace VKRemove.Data
{
	public class WorkResult
    {
		public enum WorkResultCode
		{
			CaptchaFailed,
			TooManyRequests,
			UnknownError,
			WorkCompleted
		}

		public WorkResultCode Code
		{
			get;
			set;
		}

		public string Message
		{
			get;
			set;
		}
    }
}
