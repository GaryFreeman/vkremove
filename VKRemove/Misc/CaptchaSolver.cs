﻿using System;
using System.Windows;
using VkNet.Utils.AntiCaptcha;
using VKRemove.UI;

namespace VKRemove.Misc
{
    public class CaptchaSolver : ICaptchaSolver
    {
        private Window mainWindow;
        private bool captchaIsFalse = false;
        private object captchaIsFalseMutex = new object();

        public CaptchaSolver(Window mainWindow)
        {
            this.mainWindow = mainWindow;
        }

        public string Solve(string url)
        {
            string captchaKey = null;

            mainWindow.Dispatcher.Invoke(delegate() {
                var captchaWindow = new CaptchaWindow();
                captchaWindow.ImageUri = new Uri(url, UriKind.Absolute);
                captchaWindow.Owner = mainWindow;
                captchaWindow.ShowInTaskbar = false;
                lock (captchaIsFalseMutex)
                {
                    captchaWindow.CaptchaIsFalseLabel.Visibility = captchaIsFalse ? Visibility.Visible : Visibility.Collapsed;
                }
                captchaWindow.ShowDialog();

                captchaKey = captchaWindow.CaptchaKeyField.Text;
            });

            return captchaKey;
        }
        
        public void CaptchaIsFalse()
        {
            lock (captchaIsFalseMutex)
            {
                captchaIsFalse = true;
            }
        }

        public void ResetCaptchaIsFalse()
        {
            lock (captchaIsFalseMutex)
            {
                captchaIsFalse = false;
            }
        }
    }
}
