﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace VKRemove.UI
{
    /// <summary>
    /// Interaction logic for CaptchaWindow.xaml
    /// </summary>
    public partial class CaptchaWindow : Window
    {
        public CaptchaWindow()
        {
            InitializeComponent();
        }

        public Uri ImageUri
        {
            set
            {
                var bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.UriSource = value;
                bitmap.EndInit();

                CaptchaImage.Source = bitmap;
            }
        }

        public string CaptchaKey
        {
            get
            {
                return CaptchaKeyField.Text;
            }
        }

        private void OnOkClick(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
