﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using VkNet;
using VkNet.Enums.Filters;
using VkNet.Enums.SafetyEnums;
using VkNet.Exception;
using VkNet.Model;
using VkNet.Model.Attachments;
using VkNet.Model.RequestParams;
using VkNet.Utils;
using VKRemove.Data;
using VKRemove.Misc;

namespace VKRemove.UI
{
	public partial class MainWindow : Window
	{
		private BackgroundWorker backgroundWorker;
		private CaptchaSolver captchaSolver;
		private VkApi vk;

		public MainWindow()
		{
			InitializeComponent();

			backgroundWorker = (BackgroundWorker)FindResource("backgroundWorker");
			captchaSolver = new CaptchaSolver(this);
			vk = new VkApi(captchaSolver);
		}

		private void RemoveButtonClick(object sender, RoutedEventArgs e)
		{
			if (!CheckData()) {
				return;
			}

			if (!Authorize()) {
				return;
			}

			var removeData = new RemoveData {
				Interval = Int32.Parse(IntervalField.Text),
				PhotosLikes = RemovePhotosLikesCheckbox.IsChecked.Value,
				PostsLikes = RemovePostsLikesCheckbox.IsChecked.Value,
				SavedPhotos = RemoveSavedPhotosCheckbox.IsChecked.Value
			};

			RemoveButton.IsEnabled = false;
			CancelButton.IsEnabled = true;
			ProgressBar.IsEnabled = true;
			backgroundWorker.RunWorkerAsync(removeData);
		}

		private bool Authorize()
		{
			try {
				vk.Authorize(new ApiAuthParams {
					ApplicationId = 5493446,
					Login = Login.Text,
					Password = Password.Password,
					Settings = Settings.Friends | Settings.Photos | Settings.Wall
				});
			} catch (VkApiAuthorizationException) {
				MessageBox.Show(this, "Неверный логин и/или пароль.", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);

				return false;
			} catch (VkApiException ex) {
				MessageBox.Show(this, ex.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);

				return false;
			}

			return true;
		}

		private bool CheckData()
		{
			if (String.IsNullOrEmpty(Login.Text.Trim())) {
				MessageBox.Show(this, "Введите логин.", "Внимание", MessageBoxButton.OK, MessageBoxImage.Exclamation);

				return false;
			} else if (String.IsNullOrEmpty(Password.Password.Trim())) {
				MessageBox.Show(this, "Введите пароль.", "Внимание", MessageBoxButton.OK, MessageBoxImage.Exclamation);

				return false;
			} else if (!RemoveSavedPhotosCheckbox.IsChecked.Value && !RemovePhotosLikesCheckbox.IsChecked.Value &&
				!RemovePostsLikesCheckbox.IsChecked.Value) {
				MessageBox.Show(this, "Выберите хотя бы один пункт для удаления.", "Внимание", MessageBoxButton.OK, MessageBoxImage.Exclamation);

				return false;
			} else if (String.IsNullOrEmpty(IntervalField.Text)) {
				MessageBox.Show(this, "Установите интервал запросов.", "Внимание", MessageBoxButton.OK, MessageBoxImage.Exclamation);

				return false;
			} else if (Int32.Parse(IntervalField.Text) > 1000) {
				MessageBox.Show(this, "Интервал запросов не может превышать 1 000 мс (1 секунду).", "Внимание", MessageBoxButton.OK, MessageBoxImage.Exclamation);

				return false;
			}

			return true;
		}

		private void DoWork(object sender, DoWorkEventArgs e)
		{
			try {
				var removeData = e.Argument as RemoveData;

				byte partsCount = 0;
				if (removeData.PhotosLikes) {
					++partsCount;
				}
				if (removeData.PostsLikes) {
					++partsCount;
				}
				if (removeData.SavedPhotos) {
					++partsCount;
				}

				double partRatio = 1.0 / partsCount;
				int doneProgress = 0;
				int partProgress = 0;

				if (removeData.PostsLikes) {
					var posts = vk.Fave.GetPosts(100);
					var count = posts.Count;
					var postsCollections = new List<VkCollection<Post>> {
						posts
					};

					posts = vk.Fave.GetPosts(100, count);
					count += posts.Count;
					while (posts.Count > 0) {
						postsCollections.Add(posts);

						if (backgroundWorker.CancellationPending) {
							e.Cancel = true;
							return;
						}

						posts = vk.Fave.GetPosts(100, count);
						count += posts.Count;
					}

					var allPosts = postsCollections.SelectMany(pc => pc);
					int i = 0;
					foreach (var post in allPosts) {
						vk.Likes.Delete(LikeObjectType.Post, post.Id.Value, post.OwnerId);	

						++i;
						if (backgroundWorker.WorkerReportsProgress) {
							var progress = (int)Math.Round(i / (double)allPosts.Count() * partRatio);
							backgroundWorker.ReportProgress(progress);
							partProgress = progress;
						}

						if (backgroundWorker.CancellationPending) {
							e.Cancel = true;
							return;
						}

						if (removeData.Interval > 0) {
							Thread.Sleep(removeData.Interval);
						}
					}

					doneProgress += partProgress;
				}

				partProgress = 0;
				if (removeData.PhotosLikes) {
					var photos = vk.Fave.GetPhotos(100);
					var count = photos.Count;
					var photosCollections = new List<VkCollection<Photo>> {
						photos
					};

					photos = vk.Fave.GetPhotos(100, count);
					count += photos.Count;
					while (photos.Count > 0) {
						photosCollections.Add(photos);

						if (backgroundWorker.CancellationPending) {
							e.Cancel = true;
							return;
						}

						photos = vk.Fave.GetPhotos(100, count);
						count += photos.Count;
					}

					var allPhotos = photosCollections.SelectMany(pc => pc);
					int i = 0;
					foreach (var photo in allPhotos) {
						vk.Likes.Delete(LikeObjectType.Photo, photo.Id.Value, photo.OwnerId);

						++i;
						if (backgroundWorker.WorkerReportsProgress) {
							var progress = (int)Math.Round(i / (double)allPhotos.Count() * partRatio);
							backgroundWorker.ReportProgress(progress + doneProgress);
							partProgress = progress;
						}

						if (backgroundWorker.CancellationPending) {
							e.Cancel = true;
							return;
						}

						if (removeData.Interval > 0) {
							Thread.Sleep(removeData.Interval);
						}
					}

					doneProgress += partProgress;
				}

				if (removeData.SavedPhotos) {
					var photos = vk.Photo.Get(new PhotoGetParams {
						OwnerId = vk.UserId.Value,
						AlbumId = PhotoAlbumType.Saved,
						Count = 1000
					});
					var count = photos.Count;
					var photosCollections = new List<VkCollection<Photo>> {
						photos
					};

					photos = vk.Photo.Get(new PhotoGetParams {
						OwnerId = vk.UserId.Value,
						AlbumId = PhotoAlbumType.Saved,
						Count = 1000,
						Offset = (ulong)count
					});
					count += photos.Count;
					while (photos.Count > 0) {
						photosCollections.Add(photos);

						if (backgroundWorker.CancellationPending) {
							e.Cancel = true;
							return;
						}

						photos = vk.Photo.Get(new PhotoGetParams {
							OwnerId = vk.UserId.Value,
							AlbumId = PhotoAlbumType.Saved,
							Count = 1000,
							Offset = (ulong)count
						});
						count += photos.Count;
					}

					var allPhotos = photosCollections.SelectMany(pc => pc);
					int i = 0;
					foreach (var photo in allPhotos) {
						vk.Photo.Delete((ulong)photo.Id.Value);

						++i;
						if (backgroundWorker.WorkerReportsProgress) {
							var progress = (int)Math.Round(i / (double)allPhotos.Count() * partRatio);
							backgroundWorker.ReportProgress(progress + doneProgress);
						}

						if (backgroundWorker.CancellationPending) {
							e.Cancel = true;
							return;
						}

						if (removeData.Interval > 0) {
							Thread.Sleep(removeData.Interval);
						}
					}
				}

				e.Result = new WorkResult {
					Code = WorkResult.WorkResultCode.WorkCompleted
				};
			} catch (CaptchaNeededException) {
				e.Result = new WorkResult {
					Code = WorkResult.WorkResultCode.CaptchaFailed
				};
			} catch (TooManyRequestsException) {
				e.Result = new WorkResult {
					Code = WorkResult.WorkResultCode.TooManyRequests
				};
			} catch (VkApiException ex) {
				e.Result = new WorkResult {
					Code = WorkResult.WorkResultCode.UnknownError,
					Message = ex.Message
				};
			}
		}

		private void OnWorkComplete(object sender, RunWorkerCompletedEventArgs e)
		{
			RemoveButton.IsEnabled = true;
			CancelButton.IsEnabled = false;
			ProgressBar.IsEnabled = false;
			ProgressBar.Value = 0;

			var workResult = e.Result as WorkResult;
			if (e.Cancelled) {
				MessageBox.Show(this, "Удаление отменено.", "Информация", MessageBoxButton.OK, MessageBoxImage.Information);
			} else if (workResult.Code == WorkResult.WorkResultCode.CaptchaFailed) {
				MessageBox.Show(this, "Вы превысили максимальное количество попыток для ввода капчи. Удаление остановлено.", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Exclamation);
			} else if (workResult.Code == WorkResult.WorkResultCode.TooManyRequests) {
				MessageBox.Show(this, "Превышено максимальное количество запросов в секунду. Попробуйте увлеличить интервал. Удаление остановлено.", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Exclamation);
			} else if (workResult.Code == WorkResult.WorkResultCode.UnknownError) {
				MessageBox.Show(this, $"Неизвестная ошибка: {workResult.Message}", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
			} else {
				MessageBox.Show(this, "Удаление успешно завершено.", "Информация", MessageBoxButton.OK, MessageBoxImage.Information);
			}
		}

		private void OnCancelClick(object sender, RoutedEventArgs e)
		{
			backgroundWorker.CancelAsync();
		}

		private void OnProgressChanged(object sender, ProgressChangedEventArgs e)
		{
			ProgressBar.Value = e.ProgressPercentage;
		}

		private void IntervalField_PreviewKeyDown(object sender, KeyEventArgs e)
		{
		var @char = e.Key.ToChar();
			e.Handled = !((e.Key >= Key.D0 && e.Key <= Key.D9) || (e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9)) &&
				@char != '\0' && @char != '\b' && @char != '\r' && e.Key != Key.Tab;
		}

		private void IntervalField_PreviewTextInput(object sender, TextCompositionEventArgs e)
		{
			var regex = new Regex(@"[^0-9]+");
			e.Handled = regex.IsMatch(e.Text);
		}
	}
}
